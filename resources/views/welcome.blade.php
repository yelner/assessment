<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>



        <!-- Fonts -->
        

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <style>
            body {
                height: 100vh;
                
            }

        #map {
            height: 100vh; width: 100%;
        }
            #zoomtext
        {
            transform: scale(1);
            transition: transform 0.2s ease-in-out;
        }

           
        </style>
    </head>
    <body >
    <div class="container">
        <section style="padding:10px; 2px;">
            
            <label for="lat">Latitude</label> &nbsp;
            <input type="text" id="lat" name="lat" id="lat" value=""> &nbsp; 
            <label for="lng">Longitude:</label> &nbsp; 
            <input type="text" id="lng" name="lng" id="lng" value=""> &nbsp; &nbsp;
            <button id="showMarker" value="Submit" > Show marker</button>
            
            <div id="map" style="width: auto; height: 550px; position: relative; overflow: hidden;"></div>
        </section>
    </div>
<script src="js/script.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJeH0Gv_ob_w3KUp22q_JoJUNw2kGXQs&callback=initMap" async defer></script>
  </body>
    </body>
</html>
