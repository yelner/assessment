
function initMap(){
    navigator.geolocation.getCurrentPosition(success, error);
}
var map = "";
function success(pos) {
    var crd = pos.coords;
    latitud = crd.latitude
    longitud = crd.longitude  
    var marker;
    
    map = new google.maps.Map(document.getElementById("map"), {
        center: {lat: latitud, lng: longitud},
        zoom: 14
    });
    const trexMarker = new google.maps.Marker({
        position: {lat: latitud, lng: longitud},
        map: map,
        title: 'Stan the T-Rex'
      });

    var iw = new google.maps.InfoWindow({
	    content: "Home For Sale"
	});

    google.maps.event.addListener(map, "click", (e) => {
    	
    	placeMarker(e.latLng);
	});

	function placeMarker(location) {
			
		     marker = new google.maps.Marker({
		        position: location, 
		       // label: { text: 'Lat: '+ location.lat() + '  Lng: ' + location.lng()},
		        map: map
		    });
	}
	document.getElementById("showMarker").addEventListener("click", showMarkerOnMap);

	function showMarkerOnMap()
	{  
		var inputLat = document.getElementById('lat').value;
		var inputLng = document.getElementById('lng').value;
		var myLatlng = new google.maps.LatLng(parseFloat(inputLat),parseFloat(inputLng));
		var marker = new google.maps.Marker({
	        position: myLatlng, 
	        map: map
	    });
	}
};

function error(err) {
  document.getElementById("map").innerHTML = ('ERROR(' + err.code + '): ' + err.message);
};


